from django.shortcuts import render, redirect

from api.forms import ScraperForm
from api.models import Scraper


def scraper(request):
    for scraper in Scraper.objects.all():
        scraper.update_last_value()
    c = {}
    if request.method == 'POST':
        c['form'] = ScraperForm(data=request.POST)
        c['form'].save()
    else:
        c['form'] = ScraperForm()
    c['scrapers'] = Scraper.objects.all()
    return render(request, 'list.html', c)


def update_scraper(request):
    c = {}
    if request.method == 'POST':
        c['form'] = ScraperForm(data=request.POST, is_update=True)
        c['form'].save()
    else:
        if request.GET.get('delete', '').isdigit():
            Scraper.objects.filter(id=request.GET['delete']).delete()
            return redirect(update_scraper)
        c['form'] = ScraperForm(is_update=True)
    c['scrapers'] = Scraper.objects.all()
    return render(request, 'update_list.html', c)
