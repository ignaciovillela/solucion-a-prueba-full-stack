from django import forms

from api.models import Scraper
from api.utils import get_data


class ScraperForm(forms.Form):
    money = forms.IntegerField(widget=forms.Select(choices=Scraper.MONEY_CHOICES, attrs={'class': 'form-control'}))
    frequency = forms.IntegerField(min_value=1, max_value=30)

    def __init__(self, *args, **kwargs):
        self.is_update = kwargs.pop('is_update', False)
        super().__init__(*args, **kwargs)
        self.fields['frequency'].widget.attrs['class'] = 'form-control'
        if self.is_update:
            scraper_moneys = set(Scraper.objects.values_list('money', flat=True))
            self.fields['money'].widget.choices = [(k, v) for k, v in Scraper.MONEY_CHOICES if k in scraper_moneys]

    def clean_money(self):
        money = self.cleaned_data['money']
        if not self.is_update and Scraper.objects.filter(money=money).exists():
            raise forms.ValidationError('Ya existe un scraper para esta moneda')
        return money

    def is_valid(self):
        self._errors = None
        return super().is_valid()

    def save(self):
        if self.is_valid():
            if self.is_update:
                scraper = Scraper.objects.get(money=self.cleaned_data['money'])
                scraper.frequency = self.cleaned_data['frequency']
            else:
                scraper = Scraper.objects.create(money=self.cleaned_data['money'],
                                                 last_value=0,
                                                 frequency=self.cleaned_data['frequency'])
            scraper.last_value = get_data()[scraper.get_money_display()]
            scraper.save()
            return scraper
