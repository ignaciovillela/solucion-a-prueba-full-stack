import re
import threading

import requests


def get_data():
    name_re = re.compile(r'class="cmc-link">([a-zA-Z0-9_\-. ]*)</a>')
    value_re = re.compile(r'class="cmc-link">\$([0-9,.]+)</a>')

    r = requests.get('https://coinmarketcap.com/all/views/all/')
    table = r.text.split('<div class="cmc-table__table-wrapper-outer">')[1]
    tbody = table.split('<tbody>')[1].split('</tbody>')[0]
    rows = tbody.split('</tr>')[:-1]
    moneys = {}
    for e, row in enumerate(rows):
        cells = row.split('</td>')
        name_cell = cells[1]
        money_name = name_re.search(name_cell).group(1)
        value_cell = cells[4]
        value = float(value_re.search(value_cell).group(1).replace(',', ''))
        moneys[money_name] = value
    return moneys


def get_thread_names():
    return [x.name for x in threading.enumerate()]
