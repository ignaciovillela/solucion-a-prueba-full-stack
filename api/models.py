import datetime
import threading
from time import sleep

from django.db import models

from api.utils import get_data, get_thread_names


class Scraper(models.Model):
    MONEY_CHOICES = ((1, 'Bitcoin'), (2, 'Ethereum'), (3, 'Tether'))
    money = models.IntegerField(choices=MONEY_CHOICES)
    last_value = models.FloatField()
    frequency = models. IntegerField()
    created = models.DateTimeField(default=datetime.datetime.now())

    def update_last_value(self):
        thread_name = 'Scraper-{id}'.format(id=self.id)
        if thread_name in get_thread_names():
            return None

        def func(scraper):
            while True:
                try:
                    scraper.refresh_from_db()
                except Scraper.DoesNotExist:
                    break
                scraper.last_value = get_data()[scraper.get_money_display()]
                scraper.save()
                sleep(scraper.frequency)
        thread = threading.Thread(target=func, args=[self], name=thread_name)
        thread.start()
        return thread
